<?php
namespace SBStrategy;

class NumNode extends Node
{

    protected $value;

    /**
     * @param array $position
     * @param int|float $value
     * @return NumNode
     */
    public static function create(array $position, $value)
    {
        return new static($position, $value);
    }

    /**
     * NumNode constructor.
     * @param array $position
     * @param int|float $value
     */
    protected function __construct(array $position, $value)
    {
        $this->position = $position;
        $this->value = (float) $value;
    }

    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return (float) $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        return ['value' => 'num'];
    }
}
