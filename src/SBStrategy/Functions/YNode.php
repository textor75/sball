<?php
namespace SBStrategy\Functions;

use SBObject\Position;
use SBStrategy\FunctionNode;
use SBStrategy\Strategy;
use SBUtility\Game;

class YNode extends FunctionNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        $y = $this->children[0]->play($strategy)->y;

        return $strategy->inverse ? Game::SIZE - $y : $y;
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        if (count($this->children) < 1) {
            $this->throwException('Function Y does not have any parameter');
        }
        $position = $this->children[0]->test();
        if (empty($position['object']) || !is_a($position['object'], Position::class, true)) {
            $this->throwException('The parameter of function Y must be position', [], $this->children[0]->getPosition());
        }

        return ['value' => 'num'];
    }
}
