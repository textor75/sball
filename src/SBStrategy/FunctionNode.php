<?php
namespace SBStrategy;

use SBUtility\Game;

abstract class FunctionNode extends ContainerNode
{
//@todo
//distance
//between
//closest
//my_closest
//its_closest

    /**
     * @param array $position
     * @param string|null $name
     * @return FunctionNode ContainerNode
     * @throws ParseException
     */
    public static function create(array $position, $name = null)
    {
        if (!$name) {
            throw ParseException::create(
                'Calling function without name (Line %line, Column %col)',
                ['%line' => $position[0], '%col' => $position[1]]
            );
        }
        $className = str_replace(
            ' ',
            '',
            ucwords(str_replace('_', ' ', $name))
        ) . 'Node';
        $class = __NAMESPACE__ . '\\Functions\\' . $className;
        if (!class_exists($class)) {
            $file = __DIR__ . '/Functions/' . $className . '.php';
            if (!file_exists($file)) {
                throw ParseException::create(
                    'Unknown name "%name" for function (Line %line, Column %col)',
                    ['%line' => $position[0], '%col' => $position[1],'%name' => $name]
                );
            }
            include_once $file;
        }

        return new $class($position);
    }

    /**
     *
     * @param Strategy $strategy
     * @param int $n
     * @return float
     */
    protected function getScalarArg(Strategy $strategy, $n)
    {
        $arg = $this->children[$n]->play($strategy);
        if (is_object($arg)) {
            $arg = ($arg->x + $arg->y) / 2;
            if ($strategy->inverse) {
                $arg = Game::SIZE - $arg;
            }
        }

        return $arg;
    }

}
