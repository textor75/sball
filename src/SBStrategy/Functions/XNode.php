<?php
namespace SBStrategy\Functions;

use SBStrategy\FunctionNode;
use SBStrategy\ParseException;
use SBStrategy\Strategy;
use SBUtility\Game;
use SBObject\Position;

class XNode extends FunctionNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        $x = $this->children[0]->play($strategy)->x;

        return $strategy->inverse ? Game::SIZE - $x : $x;
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        if (count($this->children) < 1) {
            $this->throwException('Function X does not have any parameter');
        }
        $position = $this->children[0]->test();
        if (empty($position['object']) || !is_a($position['object'], Position::class, true)) {
            $this->throwException('The parameter of function X must be position', [], $this->children[0]->getPosition());
        }

        return ['value' => 'num'];
    }
}
