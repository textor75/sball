<?php
namespace SBStrategy\Functions;

use SBStrategy\FunctionNode;
use SBStrategy\Strategy;
use SBStrategy\ParseException;

use SBObject\Player;
use SBObject\Position;

use SBUtility\Utility;

class BetweenNode extends FunctionNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        $pos1 = $this->children[0]->play($strategy)->getPosition();
        $pos2 = $this->children[1]->play($strategy)->getPosition();
        $k = .5;
        if (isset($this->children[2])) {
            $k = $this->children[2]->play($strategy);
        }

        $between = Utility::between($pos1, $pos2, $k);

        return Position::create($between[0], $between[1]);
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        if (count($this->children) < 2) {
            $this->throwException('Function BETWEEN must have 2 or 3 parameters');
        }

        for ($i = 0; $i < 2; $i++) {
            $position = $this->children[$i]->test();
            if (
                empty($position['object']) ||
                !is_a($position['object'], Position::class, true)
            ) {
                $this->throwException(
                    'The parameter ' . ($i + 1). ' of function BETWEEN must be position',
                    [],
                    $this->children[$i]->getPosition()
                );
            }
        }

        if (isset($this->children[2])) {
            $k = $this->children[2]->test();
            if (!isset($k['value']) || $k['value'] !== 'num') {
                $this->throwException('The 3d argument of BETWEEN must be numeral', [], $this->children[2]->getPosition());
            }
        }

        return ['object' => Position::class];
    }
}
