<?php
namespace SBStrategy\Objects;

use SBStrategy\ObjectNode;
use SBStrategy\Strategy;

use SBObject\Player;

class My1Node extends ObjectNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return $strategy->my[0];
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        return [
            'object' => Player::class,
            'type' => 'my',
        ];
    }
}
