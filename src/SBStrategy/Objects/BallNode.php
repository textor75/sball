<?php
namespace SBStrategy\Objects;

use SBStrategy\ObjectNode;
use SBStrategy\Strategy;

use SBObject\Ball;

class BallNode extends ObjectNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return $strategy->ball;
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        return ['object' => Ball::class];
    }
}
