<?php
namespace SBStrategy\Objects;

use SBStrategy\ObjectNode;
use SBStrategy\Strategy;

use SBObject\Player;

class Its1Node extends ObjectNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return $strategy->its[0];
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        return [
            'object' => Player::class,
            'type' => 'its',
        ];
    }
}
