<?php
namespace SBStrategy;

use SBObject\Ball;
use SBObject\Player;
use SBUtility\Game;

class Strategy
{
    /** @var Player[] $my */
    public $my;
    /** @var Player[] $its */
    public $its;
    /** @var Ball $ball */
    public $ball;

    /** @var Game $game */
    public $game;

    public $inverse = false;

    public $vars = [];

    /**
     * @var Node[]
     */
    private $children = [];

    /**
     * @param Node $node
     * @throws ParseException
     */
    public function addChild(Node $node)
    {
        $this->children[] = $node;
        $node->setParent($this);
    }

    /**
     * @return Strategy
     */
    public static function create()
    {
        return new self();
    }

    /**
     * Strategy constructor.
     */
    private function __construct()
    {

    }

    /**
     * Запускаеит "проигрывание" стратегию
     */
    public function play()
    {
        foreach ($this->children as $child) {
            $child->play($this);
        }
    }

    /**
     * @param Game $game
     * @param Ball $ball
     * @param Player[] $players
     * @param bool $inverse
     * @return $this
     */
    public function setObjects($game, $ball, $players, $inverse = false)
    {
        if ($this->inverse = (bool) $inverse) {
            $this->my = [$players[2], $players[3]];
            $this->its = [$players[0], $players[1]];
        } else {
            $this->my = [$players[0], $players[1]];
            $this->its = [$players[2], $players[3]];
        }

        $this->game = $game;
        $this->ball = $ball;

        return $this;
    }

    /**
     * Запускает проверку стратегии
     * @throws ParseException
     */
    public function test()
    {
        foreach ($this->children as $child) {
            $child->test();
        }
    }

}
