<?php
namespace SBStrategy;

abstract class ObjectNode extends Node
{
//@todo
//my
//its
//time
//my_gate
//its_gate
//width
//height
//disc

    /**
     * Node constructor.
     * @param array $position
     */
    protected function __construct(array $position)
    {
        $this->position = $position;
    }

    /**
     * @param array $position
     * @param string $name
     * @return ObjectNode
     * @throws ParseException
     */
    public static function create(array $position, $name)
    {
        $className = str_replace(
            ' ',
            '',
            ucwords(str_replace('_', ' ', $name))
        ) . 'Node';
        $class = __NAMESPACE__ . '\\Objects\\' . $className;
        if (!class_exists($class)) {
            $file = __DIR__ . '/Objects/' . $className . '.php';
            if (!file_exists($file)) {
                throw ParseException::create(
                    'Unknown name "%name" for object (Line %line, Column %col)',
                    ['%line' => $position[0], '%col' => $position[1], '%name' => $name]
                );
            }
            include_once $file;
        }

        return new $class($position);
    }

    public function test()
    {

    }
}
