<?php
namespace SBStrategy\Functions;

use SBStrategy\FunctionNode;
use SBStrategy\ParseException;
use SBStrategy\Strategy;

class IfNode extends FunctionNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        $condition = $this->children[0]->play($strategy);
        $result = null;
        if (isset($this->children[2])) {
            $result = $this->children[$condition ? 1 : 2]->play($strategy);
        } elseif ($condition) {
            $this->children[1]->play($strategy);
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        if (count($this->children) < 2) {
            $this->throwException('IF must have 2 or 3 parameters');
        }
        $this->children[0]->test();
        $value1 = $this->children[1]->test();
        if (!isset($this->children[2])) {
            return ['value' => null];
        }

        $value2 = $this->children[2]->test();

        if (isset($value1['value'])) {
            if (!isset($value2['value']) || $value1['value'] !== $value2['value']) {
                $this->throwException('The both results of IF must have the same type of value');
            }
        } elseif (isset($value1['object'])) {
            if (!isset($value2['object'])) {
                $this->throwException('The both results of IF must be of union type');
            }
            $parents1 = [$value1['object'] => $value1['object']] + class_parents($value1['object']);
            $parents2 = [$value2['object'] => $value2['object']] + class_parents($value2['object']);
            $intersect = array_intersect($parents1, $parents2);
            if (empty($intersect)) {
                $this->throwException('The both results of IF must extend common class');
            }
            $value1['object'] = reset($intersect);
            if (isset($value1['type']) && (!isset($value2['type']) || $value1['type'] !== $value2['type'])) {
                $value1['type'] = 'unknown';
            }
        }

        return $value1;
    }
}
