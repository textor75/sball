<?php
namespace SBStrategy;

class Parser
{
    const BR = "\n";

    private $rawCode;
    private $code;
    private $length;
    private $index;
    private $char;
    private $strategy;
    private $line;
    private $column;
    /**
     * @var Strategy|ContainerNode $currentObject
     */
    private $currentObject;

    /**
     * @param string $code
     * @return Parser
     */
    public static function create($code)
    {
        return new self($code);
    }

    /**
     * Parser constructor.
     * @param string $code
     */
    private function __construct($code)
    {
        $this->rawCode = preg_replace('/(\\r\\n|\\r|\\n)/sm', self::BR, $code);
        $this->code = mb_strtolower($this->rawCode);
        $this->length = mb_strlen($this->code);
    }

    /**
     * @return Strategy
     * @throws ParseException
     */
    public function parse()
    {
        if (isset($this->strategy)) {
            return $this->strategy;
        }

        $this->strategy = $this->currentObject = Strategy::create();
        $this->index = 0;
        $this->line = 1;
        $this->column = 1;

        try {
            do {
                $this->plunk();
            } while ($this->next());
        } catch (CodeEndException $e) {

        } catch (ParseException $e) {
            throw $e;
        } catch (\Exception $e) {
            $this->throwException($e->getMessage());
        }
        return $this->strategy;
    }

    /**
     * Перебор кода
     * @throws ParseException
     */
    private function plunk()
    {
        if ($this->checkCharIsEmpty()) {
            return;
        }
        $position = $this->getPosition();
        if ($this->isChar('"')) {
            $name = '';

            $this->next();
            while(!$this->isChar('"')) {
                $this->next();
            }
        } elseif ($this->isChar('@')) {
            $name = '';

            $this->next();
            while($this->checkCharIsChar()) {
                $name .= $this->getChar();
                $this->next();
            }
            $this->currentObject->addChild(ObjectNode::create($position, $name));
            $this->back();
        } elseif ($this->isChar('{')) {
            $object = ContainerNode::create($position);
            $this->currentObject->addChild($object);
            $this->currentObject = $object;
        } elseif ($this->isChar('}')) {
            if (!($this->currentObject && is_a($this->currentObject, ContainerNode::class))) {
                $this->throwException('Unexpected closing char');
            }
            $this->currentObject->close();
            $this->currentObject = $this->currentObject->getParent();
        } elseif ($this->checkCharIsNumber() || $this->isChar('-')) {
            $num = $this->getChar();
            if ($num === '-') {
                $this->next();
                if (!$this->checkCharIsNumber()) {
                    $this->throwException('Unknown symbol "%symbol"', ['%symbol' => $this->getChar()]);
                }
                $num .= $this->getChar();
            }
            $this->next();
            while($this->checkCharIsNumber()) {
                $num .= $this->getChar();
                $this->next();
            }
            if ($this->isChar('.')) {
                $num .= $this->getChar();
                $this->next();
                while($this->checkCharIsNumber()) {
                    $num .= $this->getChar();
                    $this->next();
                }
            }
            $float = (float) $num;
            $this->currentObject->addChild(NumNode::create($position, (float) $num));
            $this->back();
        } elseif ($this->checkCharIsChar()) {
            $name = $this->getChar();
            $this->next();
            while($this->checkCharIsChar()) {
                $name .= $this->getChar();
                $this->next();
            }
            while ($this->checkCharIsEmpty()) {
                $this->next();
            }
            if ($this->getChar() !== '{') {
                $this->throwException('Unknown symbol "%symbol"', ['%symbol' => $this->getChar()]);
            }
            $object = FunctionNode::create($position, $name);
            $this->currentObject->addChild($object);
            $this->currentObject = $object;
        }
    }

    /**
     * @return string
     */
    private function getChar()
    {
        if (!isset($this->char)) {
            $this->char = mb_substr($this->code, $this->index, 1);
        }

        return $this->char;
    }

    /**
     * @param string $char
     * @return bool
     */
    private function isChar($char)
    {
        return $this->getChar() === (string) $char;
    }

    /**
     * @return bool
     */
    private function checkCharIsLetter()
    {
        $char = $this->getChar();

        return $char >= 'a' && $char <= 'z';
    }

    /**
     * @return bool
     */
    private function checkCharIsNumber()
    {
        $char = $this->getChar();

        return $char >= '0' && $char <= '9';
    }

    /**
     * @return bool
     */
    private function checkCharIsChar()
    {
        return $this->checkCharIsLetter() || $this->checkCharIsNumber() || in_array($this->getChar(), ['_']);
    }

    /**
     * @return bool
     */
    private function checkCharIsBreak()
    {
        return in_array($this->getChar(), ["\n", "\r"]);
    }

    /**
     * @return bool
     */
    private function checkCharIsSpace()
    {
        return in_array($this->getChar(), [" ", "\t"]);
    }

    /**
     * @return bool
     */
    private function checkCharIsEmpty()
    {
        return $this->checkCharIsBreak() || $this->checkCharIsSpace();
    }

    /**
     * @return bool
     * @throws ParseException
     */
    private function next()
    {
        $result = true;
        $this->index++;
        if ($this->index >= $this->length) {
            $this->index = $this->length;
            throw CodeEndException::create();
        }
        $this->char = null;
        if ($this->checkCharIsBreak()) {
            $this->line++;
            $this->column = 0;
        } else {
            $this->column++;
        }

        return $result;
    }

    private function back()
    {
        $result = true;
        $breakUp = $this->checkCharIsBreak();
        $this->index--;
        if ($this->index < 0) {
            $this->index = 0;
            $result = false;
        } elseif ($breakUp) {
            $this->line--;
            $lastBreak = mb_strrpos(mb_substr($this->code, 0, $this->index), self::BR) ?: 0;
            $this->column = $this->index - $lastBreak;
        } else {
            $this->column--;
        }
        $this->char = null;


        $back = mb_substr($this->code, $this->index, 1);
        return $result;
    }

    private function getPosition()
    {
        return [$this->line, $this->column];
    }

    /**
     * @param string $message
     * @param array $params
     * @throws ParseException
     */
    private function throwException($message, array $params = [], $position = null)
    {
        if (!$position) {
            $position = $this->getPosition();
        }
        throw ParseException::create(
            $message . sprintf(' (Line %d,  Column %d)', $position[0], $position[1]),
            $params
        );
    }

}
