<?php
namespace SBStrategy;

class ContainerNode extends Node
{
    /**
     * @var Node[]
     */
    protected $children = [];

    /**
     * Node constructor.
     * @param array $position
     */
    protected function __construct(array $position)
    {
        $this->position = $position;
    }

    /**
     * @param array $position
     * @param null|string $name
     * @return ContainerNode
     */
    public static function create(array $position, $name = null)
    {
        return new static($position);
    }

    /**
     * @param Node $node
     * @throws ParseException
     */
    public function addChild(Node $node)
    {
        $this->children[] = $node;
        $node->setParent($this);
    }

    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        foreach ($this->children as $child) {
            $child->play($strategy);
        }

        return null;
    }

    public function close()
    {
    }

    public function test()
    {
        foreach ($this->children as $child) {
            $child->test();
        }

        return ['value' => null];
    }
}
