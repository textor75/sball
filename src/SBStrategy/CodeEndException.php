<?php
namespace SBStrategy;

use Exception;

class CodeEndException extends Exception
{
    /**
     * @param string $message
     * @param array $params
     * @return ParseException
     */
    public static function create($message = 'End of code', array $params = [])
    {
        foreach ($params as $key => $value) {
            $message = str_replace($key, (string) $value, $message);
        }

        return new static($message);
    }
}
