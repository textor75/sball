<?php
namespace SBObject;

use SBUtility\Utility;

class Ball extends Thing
{
    const KICK_SPEED = 20;

    const KICK_TOP = 0;
    const KICK_BOTTOM = 1;
    const KICK_LEFT = 2;
    const KICK_RIGHT = 3;

    const BORDER_KICK_VAL = 16;

    public function kickFromBorder($type)
    {
        switch ($type) {
            case self::KICK_LEFT:
                $this->directionX = max(self::BORDER_KICK_VAL, abs($this->directionX));
                break;
            case self::KICK_RIGHT:
                $this->directionX = -max(self::BORDER_KICK_VAL, abs($this->directionX));
                break;
            case self::KICK_TOP:
                $this->directionY = -max(self::BORDER_KICK_VAL, abs($this->directionY));
                break;
            case self::KICK_BOTTOM:
                $this->directionY = max(self::BORDER_KICK_VAL, abs($this->directionY));
                break;
        }
        $this->speed = max(self::BORDER_KICK_VAL, $this->speed);
    }


    public function setStep()
    {
        $this->fixOld();

        if (!($this->directionX || $this->directionY) || !$this->speed) {
            return $this->directionX = $this->directionY = $this->speed = 0;
        }

        $distance = sqrt(pow($this->directionX, 2) + pow($this->directionY, 2));

        if ($distance <= $this->speed) {
            $this->x += $dx;
            $this->y += $dy;
        } else {
            $this->x += $this->directionX * $this->speed / $distance;
            $this->y += $this->directionY * $this->speed / $distance;
            $distance = $this->speed;
        }

        $this->speed *= static::SLOWDOWN;
        if ($this->speed < static::MIN_SPEED) {
            $this->speed = 0;
        }

        return $distance;
    }

    public function stop()
    {
        $this->directionX = 0;
        $this->directionY = 0;
        $this->speed = 0;
    }

}
