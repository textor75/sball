<?php
namespace SBStrategy\Functions;

use SBStrategy\FunctionNode;
use SBStrategy\ParseException;
use SBStrategy\Strategy;

class NotNode extends FunctionNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return !$this->children[0]->play($strategy);
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        if (count($this->children) < 1) {
            $this->throwException('Function NOT does not have any parameter');
        }
        $this->children[0]->test();
        return ['value' => 'bool'];
    }
}
