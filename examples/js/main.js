jQuery(function($) {

    $(document).ready(function() {
        var oReq = new XMLHttpRequest();
        oReq.open('GET', 'game.dat?' + (new Date()).getTime(), true);
        oReq.responseType = 'arraybuffer';
        var normalArray = [];

        var FPS = 24;
        var TIME = 300;
        var ELEMENT_COUNT = 12;

        var duration = TIME * FPS;
        var timeOut = Math.round(100 / FPS);
        var timer = 0;
        // самый поздний файл из загруженных
        var maxKey = 0;

        var fixedStart = null;
        var loader = $('.sb-timelne__loaded');
        var timeline = $('.sb-timelne__played');

        var playing = true;

        var gameData = {};

        var sbObjects = $('.sb-field > .sb-object');

        var showGame = function() {
            timer = getTimer();

            if (typeof gameData[timer] === 'undefined') {
                setTimeout(function() { showGame(); }, timeOut);
                return;
            }

            var moment = gameData[timer];

            timeline.css('width', ((timer + 1) * 100 / duration) + '%');

            var counter = 0;
            sbObjects.each(function() {
                $(this).css({
                    left: (moment[counter] / 10) + '%',
                    top: (moment[counter + 1] / 10) + '%',
                });
                counter += 2;
            });

            if (timer < duration) {
                setTimeout(function() { showGame(); }, timeOut);
            } else {
                playing = false;
            }

        };

        var getTimer = function() {
            return Math.round(((new Date()).getTime() - fixedStart) * FPS / 1000);
        };

        var fixStart = function(fixTimer) {
            fixedStart = Math.round((new Date()).getTime() - fixTimer * 1000 / FPS);
        };

        var setTimeline = function(rate) {
            var fixTimer = Math.round(duration * rate);
            fixStart(fixTimer);
        };

        var readPack = function(count) {
            var item, value, sum, key;
            var remain = count;
            while(normalArray.length >= ELEMENT_COUNT * 2 && remain > 0) {
                item = [];
                sum = 0;
                for (var i = 0; i < ELEMENT_COUNT; i++) {
                    value = normalArray.shift() * 256;
                    value += normalArray.shift();
                    if (!i) {
                        key = value;
                        sum += value;
                    } else if (i < ELEMENT_COUNT - 1) {
                        sum += value;
                        item.push(value);
                    } else if (sum % 50000 === value) {
                        gameData[key] = item;
                        if (key > maxKey) {
                            maxKey = key;
                            loader.css('width', ((maxKey + 1) * 100 / duration) + '%');
                        }
                    }
                }
                remain--;
            }

            if (normalArray.length) {
                setTimeout(function() { readPack(count); }, 10);
            }
        };

        oReq.onload = function (oEvent) {
            var arrayBuffer = oReq.response;
            if (arrayBuffer) {
                normalArray = Array.prototype.slice.call(new Uint8Array(arrayBuffer));
                readPack(4);
            }
        };

        oReq.send(null);


        timeline.on('click', function(e) {
            var rate =  (e.pageX - $(this).offset().left) / $(this).parent().width();
            setTimeline(rate);
        });
        loader.on('click', function(e) {
            var rate =  (e.pageX - $(this).offset().left) / $(this).parent().width();
            setTimeline(rate);
        });

        fixStart(0);
        showGame();
    });

});