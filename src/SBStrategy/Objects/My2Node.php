<?php
namespace SBStrategy\Objects;

use SBStrategy\ObjectNode;
use SBStrategy\Strategy;

use SBObject\Player;

class My2Node extends ObjectNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return $strategy->my[1];
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        return [
            'object' => Player::class,
            'type' => 'my',
        ];
    }
}
