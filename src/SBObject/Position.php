<?php
namespace SBObject;

class Position extends AbstractObject
{
    public $x;
    public $y;

    public static function create($x, $y)
    {
        return new static($x, $y);
    }

    protected function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     *
     * @return array
     */
    public function getPosition()
    {
        return [$this->x, $this->y];
    }

    /**
     *
     * @param bool $round
     * @return int|float
     */
    public function getX($round = true)
    {
        return $round ? round($this->x) : $this->x;
    }

    /**
     *
     * @param bool $round
     * @return int|float
     */
    public function getY($round = true)
    {
        return $round ? round($this->y) : $this->y;
    }

}
