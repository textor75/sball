<?php
namespace SBStrategy\Functions;

use SBStrategy\FunctionNode;
use SBStrategy\Strategy;
use SBStrategy\ParseException;

use SBObject\Player;
use SBObject\Position;

use SBUtility\Utility;

class DistanceNode extends FunctionNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        $pos1 = $this->children[0]->play($strategy)->getPosition();
        $pos2 = $this->children[1]->play($strategy)->getPosition();
        return Utility::getDistance($pos1, $pos2);
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        if (count($this->children) < 2) {
            $this->throwException('Function DISTANCE must have 2 parameters');
        }

        for ($i = 0; $i < 2; $i++) {
            $position = $this->children[$i]->test();
            if (
                empty($position['object']) ||
                !is_a($position['object'], Position::class, true)
            ) {
                $this->throwException(
                    'The parameter ' . ($i + 1). ' of DISTANCE must be position',
                    [],
                    $this->children[$i]->getPosition()
                );
            }
        }

        return ['value' => 'num'];
    }
}
