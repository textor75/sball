<?php
namespace SBObject;

use SBUtility\Game;

class Player extends Thing
{
    protected $targetX;
    protected $targetY;

    const KICK_SPEED = 4;
    const SLOWDOWN = 0.6;

    protected $stepSize = 8;

    public function setTarget(Position $target)
    {
        $position = $target->getPosition();
        $this->targetX = $position[0];
        $this->targetY = $position[1];

        return $this;
    }

    public function setStep()
    {
        $this->fixOld();

        $dx = isset($this->targetX) ? $this->targetX - $this->x : 0;
        $dy = isset($this->targetY) ? $this->targetY - $this->y : 0;

        // Делаем поправку на инерцию и толчок
        $dx += $this->directionX;
        $dy += $this->directionY;

        if (!$dx && !$dy) {
            return 0;
        }
        $distance = sqrt(pow($dx, 2) + pow($dy, 2));

        if ($distance <= $this->stepSize) {
            $this->x += $dx;
            $this->y += $dy;
        } else {
            $this->x += $dx * $this->stepSize / $distance;
            $this->y += $dy * $this->stepSize / $distance;
            $distance = $this->stepSize;
        }

        $this->directionX = $dx * static::SLOWDOWN;
        $this->directionY = $dy * static::SLOWDOWN;

        return $distance;
    }

    public function clearTarget()
    {
        unset($this->targetX, $this->targetY);
        return $this;
    }
}
