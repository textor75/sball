<?php
namespace SBStrategy\Functions;

use SBStrategy\FunctionNode;
use SBStrategy\ParseException;
use SBStrategy\Strategy;
use SBObject\Position;
use SBUtility\Game;

class PositionNode extends FunctionNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        $k = $strategy->inverse ? -1 : 1;
        $a = $strategy->inverse ? Game::SIZE : 0;
        return Position::create(
            $a + $this->children[0]->play($strategy) * $k,
            $a + $this->children[1]->play($strategy) * $k
        );
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        if (count($this->children) < 2) {
            $this->throwException('POSITION must have 2 parameters');
        }
        for ($i = 0; $i < 2; $i++) {
            $value = $this->children[$i]->test();
            if (!isset($value['value']) || $value['value'] !== 'num') {
                $this->throwException('Arguments of POSITION must be numeral', [], $this->children[$i]->getPosition());
            }
        }
        return ['object' => Position::class];
    }
}
