<?php
namespace SBStrategy\Objects;

use SBStrategy\ObjectNode;
use SBStrategy\Strategy;

class MyScoreNode extends ObjectNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return $strategy->game->getScore($strategy->inverse ? 1 : 0);
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        return ['value' => 'num'];
    }
}
