<?php
namespace SBStrategy\Functions;

use SBStrategy\FunctionNode;
use SBStrategy\ParseException;
use SBStrategy\Strategy;

use SBObject\Position;

class LessNode extends FunctionNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return $this->getScalarArg($strategy, 0) < $this->getScalarArg($strategy, 1);
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        if (count($this->children) < 2) {
            $this->throwException('Function LESS must have 2 parameters');
        }
        for ($i = 0; $i < 2; $i++) {
            $value = $this->children[$i]->test();
            if (isset($value['value'])) {
                if ($value['value'] !== 'num') {
                    $this->throwException('Arguments of LESS must be scalar', [], $this->children[$i]->getPosition());
                }
            } elseif (isset($value['object'])) {
                if (!is_a($value['object'], Position::class, true)) {
                    $this->throwException('Arguments of LESS must be scalar', [], $this->children[$i]->getPosition());
                }
            } else {
                $this->throwException('Arguments of LESS must be scalar', [], $this->children[$i]->getPosition());
            }
        }
        return ['value' => 'bool'];
    }
}
