<?php
namespace SBStrategy;

interface NodeInterface
{
    /**
     * @param Strategy $strategy
     * @return mixed
     */
    public function play(Strategy $strategy);

    /**
     * @return null|array
     * @throws ParseException
     */
    public function test();
}
