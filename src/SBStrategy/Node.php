<?php
namespace SBStrategy;

abstract class Node implements NodeInterface
{
    /**
     * @var null|Node
     */
    protected $parent = null;

    /**
     * @var array $position
     */
    protected $position;

    /**
     * @param null|ContainerNode|Strategy $node
     * @throws ParseException
     */
    public function setParent($node)
    {
        if (!($node instanceof ContainerNode || $node instanceof Strategy)) {
            $this->throwException('Node %class can\'t be parent', ['%class' => get_class($node)]);
        }
        $this->parent = $node;
    }

    /**
     * @return null|Node
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return array
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $message
     * @param array $params
     * @throws ParseException
     */
    protected function throwException($message, array $params = [], $position = null)
    {
        if (!$position) {
            $position = $this->position;
        }
        throw ParseException::create(
            $message . sprintf(' (Line %d, Column %d)', $position[0], $position[1]),
            $params
        );
    }
}