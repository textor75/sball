<?php
namespace SBStrategy\Functions;

use SBStrategy\FunctionNode;
use SBStrategy\Strategy;
use SBStrategy\ParseException;

use SBObject\Player;
use SBObject\Position;

class GoNode extends FunctionNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return
            $this->children[0]->play($strategy)->setTarget(
                $this->children[1]->play($strategy)
            );
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        if (count($this->children) < 2) {
            $this->throwException('Function GO must have 2 parameters');
        }
        $player = $this->children[0]->test();
        if (
            empty($player['object']) ||
            !is_a($player['object'], Player::class, true) ||
            (empty($player['type']) || $player['type'] !== 'my')
        ) {
            $this->throwException(
                'The 1st parameter of function GO must be your player',
                [],
                $this->children[0]->getPosition()
            );
        }
        $position = $this->children[1]->test();
        if (
            empty($position['object']) ||
            !is_a($player['object'], Position::class, true)
        ) {
            $this->throwException(
                'The 2nd parameter of function GO must be position',
                [],
                $this->children[1]->getPosition()
            );
        }

        return [
            'object' => Player::class,
            'type' => 'my',
        ];
    }
}
