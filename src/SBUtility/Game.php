<?php
namespace SBUtility;

use SBObject\Player;
use SBObject\Ball;
use SBStrategy\Strategy;
use SBStrategy\Parser;
use SBUtility\Utility;

class Game extends AbstractUtility
{
    const TIME = 300;
    const FPS = 24;
    const SIZE = 1000;
    const GATE = 300;
    const D = 100;

    const FLAG_IS_AUTO = 0;

    private static $start = [
        0 => [150, 450],
        1 => [450, 150],
        2 => [850, 550],
        3 => [550, 850],
        'ball' => [500, 500],
        'my_ball' => [350, 350],
        'its_ball' => [650, 650],
    ];

    private $timer = 0;
    private $players;
    private $ball;
    private $score = [0, 0];

    private $flags = 0;

    /**
     * @var Strategy $strategy1
     */
    private $strategy1;
    /**
     * @var Strategy $strategy2
     */
    private $strategy2;

    public static function create($strategy1, $strategy)
    {
        return new self($strategy1, $strategy);
    }

    protected function __construct($strategy1, $strategy2)
    {
        $this->players = [];
        for ($i = 0; $i < 4; $i++) {
            $this->players[] = Player::create(self::$start[$i][0], self::$start[$i][1]);
        }

        $this->ball = Ball::create(self::$start['ball'][0], self::$start['ball'][1]);
        $this->strategy1 = Parser::create($strategy1)->parse()->setObjects($this, $this->ball, $this->players);
        $this->strategy2 = Parser::create($strategy2)->parse()->setObjects($this, $this->ball, $this->players, true);
    }

    public function play()
    {
        $duration = self::TIME * self::FPS;
        try {
            for ($this->timer = 0; $this->timer < $duration; $this->timer++) {
                for ($i = 0; $i < 4; $i++) {
                    $this->players[$i]->clearTarget();
                }
                $this->strategy1->play();
                $this->strategy2->play();
                $this->move();
                $this->out();
                $this->flags = 0;
            }
        } catch (\Exception $e) {

        }
        return $this;
    }

    public function getRelativeTime()
    {
        return $this->timer / self::FPS / self::TIME;
    }

    /**
     *
     * @param int $team 0/1
     * @return int
     */
    public function getScore($team)
    {
        return $this->score[$team];
    }

    private function move()
    {
        $count = count($this->players);

        for ($i1 = 0; $i1 < $count; $i1++) {
            $player = $this->players[$i1];

            if (!($player->setStep())) {
                continue;
            }

            if ($player->x < 0) {
                $player->x = 0;
            } elseif ($player->x > self::SIZE) {
                $player->x = self::SIZE;
            }
            if ($player->y + $player->y < 0) {
                $player->y = 0;
            } elseif ($player->y > self::SIZE) {
                $player->y = self::SIZE;
            }

            $accidentDistance = null;
            $accidentObject = null;

            for ($i2 = 0; $i2 < $count; $i2++) {
                $other = $this->players[$i2];

                if ($i1 === $i2) {
                    continue;
                }

                $distance = Utility::intersectionRayCircle(
                    $player->getOld(),
                    $player->getPosition(),
                    $other->getPosition(),
                    self::D
                );

                if ($distance !== null && ($accidentDistance === null || $accidentDistance > $distance)) {
                    $accidentDistance = $distance;
                    $accidentObject = $other;
                }
            }
            $distance = Utility::intersectionRayCircle(
                $player->getOld(),
                $player->getPosition(),
                $this->ball->getPosition(),
                self::D
            );

            if ($distance !== null && ($accidentDistance === null || $accidentDistance > $distance)) {
                $accidentDistance = $distance;
                $accidentObject = $this->ball;
            }

            if ($accidentObject) {
                $player->back($accidentDistance);
                $player->kickFrom($accidentObject);
                $accidentObject->kickFrom($player, 4);
            }
        }

        if ($this->ball->setStep()) {
            if ($goal = $this->checkGoal()) {
                $this->setInitialPosition($goal < 0);
                $this->score[$goal > 0 ? 1 : 0]++;
            }
        }
    }

    private function setInitialPosition($myWin)
    {
        $this->setFlag(self::FLAG_IS_AUTO, 1);

        $ballStart = [
            self::$start[$myWin ? 'its_ball' : 'my_ball'][0] + rand(-50, 50),
            self::$start[$myWin ? 'its_ball' : 'my_ball'][1] + rand(-50, 50),
        ];

        $duration = self::TIME * self::FPS;

        $initial = [
            'ball' => [$this->ball->x, $this->ball->y],
        ];

        for ($p = 0; $p < 4; $p++) {
            $this->players[$p]->clearDirection();
            $initial[$p] = [$this->players[$p]->x, $this->players[$p]->y];
        }

        $time = 1;

        $this->ball->stop();

        for ($i = 0; $i < $time + 1; $i++) {
            $this->timer++;
            if ($this->timer >= $duration) {
                break;
            }
            $k = $i / $time;
            $this->ball->x = $initial['ball'][0] + ($ballStart[0] - $initial['ball'][0]) * $k;
            $this->ball->y = $initial['ball'][1] + ($ballStart[1] - $initial['ball'][1]) * $k;

            for ($p = 0; $p < 4; $p++) {
                $this->players[$p]->x = $initial[$p][0] + (self::$start[$p][0] - $initial[$p][0]) * $k;
                $this->players[$p]->y = $initial[$p][1] + (self::$start[$p][1] - $initial[$p][1]) * $k;
            }

            $this->out();
        }

        $this->flag = 0;
    }

    private function checkGoal()
    {
        if ($this->ball->x < 0) {
            if ($this->ball->y < self::GATE) {
                return 1;
            }
            $this->ball->x = 0;
            $this->ball->kickFromBorder(Ball::KICK_LEFT);
        } elseif ($this->ball->x > self::SIZE) {
            if ($this->ball->y > self::SIZE - self::GATE) {
                return -1;
            }
            $this->ball->x = self::SIZE;
            $this->ball->kickFromBorder(Ball::KICK_RIGHT);
        }
        if ($this->ball->y + $this->ball->y < 0) {
            if ($this->ball->x < self::GATE) {
                return 1;
            }
            $this->ball->y = 0;
            $this->ball->kickFromBorder(Ball::KICK_BOTTOM);
        } elseif ($this->ball->y > self::SIZE) {
            if ($this->ball->x > self::SIZE - self::GATE) {
                return -1;
            }
            $this->ball->y = self::SIZE;
            $this->ball->kickFromBorder(Ball::KICK_TOP);
        }

        $accidentDistance = null;
        $accidentObject = null;

        $cnt = count($this->players);
        for ($i = 0; $i < $cnt; $i++) {
            $player = $this->players[$i];
            $distance = Utility::intersectionRayCircle(
                $this->ball->getOld(),
                $this->ball->getPosition(),
                $player->getPosition(),
                self::D
            );
            if ($distance !== null && ($accidentDistance === null || $accidentDistance > $distance)) {
                $accidentDistance = $distance;
                $accidentObject = $player;
            }
        }

        if ($accidentObject) {
            $this->ball->back($accidentDistance);
            $this->ball->kickFrom($accidentObject);
            $accidentObject->kickFrom($this->ball, 2);
        }

        return 0;
    }

    private function out()
    {
        $pack = chr($this->flags);

        $pack .= pack('n', $this->ball->getX());
        $pack .= pack('n', $this->ball->getY());

        for ($i = 0; $i < 4; $i++) {
            $pack .= pack('n', $this->players[$i]->getX());
            $pack .= pack('n', $this->players[$i]->getY());
        }

        $pack .= chr($this->score[0]);
        $pack .= chr($this->score[1]);

        echo $pack;

        @ob_flush();
        flush();
    }

    private function setFlag($position, $value = true)
    {
        $flag = 1 << $position;
        if ($value) {
            $this->flags |= $flag;
        } else {
            $this->flags &= ~$flag;
        }
    }

}
