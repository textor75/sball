<?php
// php game.php > game.dat
use SBUtility\Game;

include_once __DIR__ . '/../vendor/autoload.php';
include_once __DIR__ . '/../../debug.php';
\x::dev(1);

$file1 = 'simple1';
$file2 = 'simple2';

$strategy1 = file_get_contents(__DIR__ . '/strategy/' . $file1 . '.txt');
$strategy2 = file_get_contents(__DIR__ . '/strategy/' . $file2 . '.txt');

Game::create($strategy1, $strategy2)->play();