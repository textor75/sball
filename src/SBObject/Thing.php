<?php
namespace SBObject;

class Thing extends Position
{
    public $oldX;
    public $oldY;

    const MIN_SPEED = 1;
    const KICK_SPEED = 0;
    const SLOWDOWN = 0.9;

    protected $speed = 0;
    protected $directionX = 0;
    protected $directionY = 0;

    public function fixOld()
    {
        $this->oldX = $this->x;
        $this->oldY = $this->y;
    }

    public function getOld()
    {
        return [$this->oldX, $this->oldY];
    }

    public function back($diff)
    {
        $dx = $this->x - $this->oldX;
        $dy = $this->y - $this->oldY;

        $distance = sqrt(pow($dx, 2) + pow($dy, 2));

        $this->x = $this->oldX + $diff * $dx;
        $this->y = $this->oldY + $diff * $dy;
    }

    public function kickFrom(Thing $kicker, $strength = 1)
    {
        $k = static::KICK_SPEED * $strength;
        $this->directionX += ($this->x - $kicker->x) * $k;
        $this->directionY += ($this->y - $kicker->y) * $k;
        $this->speed = $k;
    }

    public function clearDirection()
    {
        $this->directionX = 0;
        $this->directionY = 0;
        return $this;
    }

}
