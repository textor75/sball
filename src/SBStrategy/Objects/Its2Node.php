<?php
namespace SBStrategy\Objects;

use SBStrategy\ObjectNode;
use SBStrategy\Strategy;

use SBObject\Player;

class Its2Node extends ObjectNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return $strategy->its[1];
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        return [
            'object' => Player::class,
            'type' => 'its',
        ];
    }
}
