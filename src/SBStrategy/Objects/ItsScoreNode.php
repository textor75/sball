<?php
namespace SBStrategy\Objects;

use SBStrategy\ObjectNode;
use SBStrategy\Strategy;

class ItsScoreNode extends ObjectNode
{
    /**
     * {@inheritdoc}
     */
    public function play(Strategy $strategy)
    {
        return $strategy->game->getScore($strategy->inverse ? 0 : 1);
    }

    /**
     * {@inheritdoc}
     */
    public function test()
    {
        return ['value' => 'num'];
    }
}
