<!DOCTYPE html>
<html>
    <head>

		<script src="//test.meum.ru/libs/jquery/jquery-latest.min.js"></script>
		<script src="//test.meum.ru/libs/three-js/three.js"></script>
		<script src="//test.meum.ru/three/js/controls/OrbitControls.js"></script>
		<script src="js/main.js"></script>
        <link href="css/style.css" type="text/css"  rel="stylesheet" />
    </head>
    <body>
        <div class="sb-container">
            <div class="sb-field">
                <div class="sb-object sb-ball"></div>
                <div class="sb-object sb-player sb-team-1" data-num="1"></div>
                <div class="sb-object sb-player sb-team-1" data-num="2"></div>
                <div class="sb-object sb-player sb-team-2" data-num="1"></div>
                <div class="sb-object sb-player sb-team-2" data-num="2"></div>
            </div>
            <div class="sb-timeline">
                <div class="sb-timelne__loaded"></div>
                <div class="sb-timelne__played"></div>
            </div>
        </div>
    </body>
</html>