<?php
namespace SBUtility;

use SBObject\Position;

class Utility
{
    public static function getDistance(array $pos1, array $pos2)
    {
        return sqrt(
            pow($pos2[0] - $pos1[0], 2)
            + pow($pos2[1] - $pos1[1], 2)
        );
    }

    public static function between(array $pos1, array $pos2, $k = .5)
    {
        return [
            $pos1[0] + ($pos2[0] - $pos1[0]) * $k,
            $pos1[1] + ($pos2[1] - $pos1[1]) * $k
        ];
    }

    /**
     * Точка пересечения луча и окружности
     * @param array $point исходящая точка луча
     * @param array $direction направление
     * @param array $center центр окружности
     * @param float $radius радиус окружности
     * @return float|null условное расстояние до пересечения
     *   (сколько отрезков point-direction до пересечения)
     */
    public static function intersectionRayCircle($point, $direction, $center, $radius)
    {
        $currentDistance = self::getDistance($direction, $center);
//        if ($currentDistance > self::getDistance($point, $direction) + $radius) {
        if ($currentDistance > $radius) {
            return null;
        }

        $x1 = $point[0] - $center[0];
        $y1 = $point[1] - $center[1];

        $x2 = $direction[0] - $center[0];
        $y2 = $direction[1] - $center[1];

        $dx = $x2 - $x1;
        $dy = $y2 - $y1;

        if (!$dx && !$dy) {
            return null;
        } elseif (!$dx) {
            if (abs($x1) > $radius) {
                return null;
            }

            $root = $x1 ? sqrt(Math.pow($radius, 2) - Math.pow($x1, 2))  : $radius;
            $min = null;

            foreach ([$root, -$root] as $y) {
                $result = ($y - $y1) / $dy;

                if (($result >= 0 || $currentDistance < $radius) && $result <= 1) {
                    if ($min === null) {
                        $min = $result;
                    } elseif ($min < 0) {
                        $min = max($min, $result);
                    } else {
                        $min = min($min, $result);
                    }
                }
            }
            return $min;
        }

        list($A, $B, $C) = self::getTwoPointsLineEquation([$x1, $y1], [$x2, $y2]);

        $x12 = self::getQuadraticEquationRoots(
            pow($A, 2) + pow($B, 2),
            2 * $A * $C,
            pow($C, 2) - pow($B * $radius, 2)
        );

        if (!$x12) {
            return null;
        }

        $min = null;
        foreach ($x12 as $x) {
            if (abs($dx) > abs($dy) || !$B) {
                $result = ($x - $x1) / $dx;
            } else {
                $y = -($A * $x + $C) / $B;
                $result = ($y - $y1) / $dy;
            }
            if (($result >= 0 || $currentDistance < $radius) && $result <= 1) {
                if ($min === null) {
                    $min = $result;
                } elseif ($min < 0) {
                    $min = max($min, $result);
                } else {
                    $min = min($min, $result);
                }
            }
        }

        return $min;
    }

    /**
     * Уровнение прямой, проходящей через 2 точки: aX + bY + c = 0
     * @param array $point1
     * @param array $point2
     * @return array
     *   [
     *     a (y1-y2),
     *     b (x2-x1),
     *     c (x1y2-x2y1),
     *   ]
     */
    public static function getTwoPointsLineEquation($point1, $point2)
    {
        return [
            $point1[1] - $point2[1],
            $point2[0] - $point1[0],
            $point1[0] * $point2[1] - $point2[0] * $point1[1],
        ];
    }

    /**
     * Решение квадратного уровнения (aX^2 + bX + C = 0)
     *   (-b ± √D) / 2a, где D = b^2 - 4ac
     * @param float $a
     * @param float $b
     * @param float $c
     * @return array|null
     */
    public static function getQuadraticEquationRoots($a, $b, $c)
    {
        if (!$a) {
            return $b ? [-$c / $b] : null;
        }

        $D = pow($b, 2) - 4 * $a * $c;
        if ($D < 0) {
            return null;
        }

        $result = [ (-$b + sqrt($D))/2/$a ];
        if ($D > 0) {
            $result[] = (-$b - sqrt($D))/2/$a;
        }

        return $result;
    }

}
